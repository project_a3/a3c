// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"

#include "Json.h"

#include "A3_Helper.generated.h"

UCLASS()
class A3_API AA3_Helper : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AA3_Helper();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// note - data parsing 
public:
	UFUNCTION(BlueprintCallable, Category = "JsonParse")
		FString		ParseToJsonDate();

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Add String Field"), Category = "JsonParse")
		AA3_Helper*	SetString(const FString& key, const FString& value);

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Add Integer Field"), Category = "JsonParse")
		AA3_Helper*	SetInt(const FString& key, const int32 value);

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Add Float Field"), Category = "JsonParse")
		AA3_Helper*	SetFloat(const FString& key, const float value);

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Add Float boolean"), Category = "JsonParse")
		AA3_Helper*	SetBool(const FString& key, const bool value);

	//note - delegate에서 받은 데이터를 json 형태로 변환하여 key값을 통해 value를 얻어오기 위함
	UFUNCTION(BlueprintCallable, Category = "JsonParse")
		AA3_Helper* JsonConvertor(const FString& data);

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Get Integer Value"), Category = "JsonParse")
		int			GetInt(const FString& key);

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Get Float Value"), Category = "JsonParse")
		float		GetFloat(const FString& key);

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Get String Value"), Category = "JsonParse")
		FString		GetString(const FString& key);

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Get Bool Value"), Category = "JsonParse")
		bool		GetBool(const FString& key);

	void ReleaseJson();

protected:
	TSharedPtr<FJsonObject>		_jsonData;
	TSharedPtr<FJsonObject>		_inputJsonData;

};

#define CHARACTERUPDATE		21


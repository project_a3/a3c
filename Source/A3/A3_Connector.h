// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"

//추가 헤더
#include <Networking.h>
#include "Runnable.h"
#include "RunnableThread.h"
#include "A3_GameMode.h"
#include "Engine.h"
#include "Json.h"

#include "A3_Connector.generated.h"

template <class T>
class CSafeQueue
{
public:
	CSafeQueue() {};
	virtual ~CSafeQueue() {};

	T Pop()
	{
		FScopeLock lock(&_cs);

		if (_queue.Num() != 0)
		{
			T queue = *_queue.GetData();
			return queue;
		}

		return NULL;
	}

	void Push(T data)
	{
		FScopeLock lock(&_cs);
		//_queue.Push(data);
		_queue.Add(data);
		//_queue.Enqueue(data);
	}

	void Erase(T data)
	{
		FScopeLock lock(&_cs);
		_queue.Remove(data);
		//_queue.Dequeue(data);
	}

	int32 Empty()
	{
		FScopeLock lock(&_cs);
		return _queue.Num();


	}

	TArray<T>&	GetQueue()
	{
		return &_queue;
	}

	void Lock()
	{
		_cs.Lock();
	}

	void UnLock()
	{
		_cs.Unlock();
	}

	TArray<T>			_queue;
	FCriticalSection	_cs;
};

struct PacketQueue
{
	int32						_type;
	TSharedPtr<FJsonObject>		_data;
};

UCLASS()
class A3_API AA3_Connector : public AActor, public FRunnable
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AA3_Connector();
	~AA3_Connector();

	UFUNCTION(BlueprintCallable, Category = "A3_Connector")
	virtual void Destroy();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//note - packet type에 따른 함수 콜을 위함
public:
	union FuncProcess
	{
		void(*funcProcessPacket)(TSharedPtr<FJsonObject> packetData);

		FuncProcess()
		{
			funcProcessPacket = NULL;
		}
	};

	//packet type 에 따른 함수 등록
	bool				FuncPacketInitialize();

	//등록된 함수 호출을 위한 함수
	bool				RecvImmediately(int32 size, char* packetData);

	UFUNCTION(BlueprintCallable, Category = "A3_Connector")
	void				ProcessPacketEvent();

protected:
	CSafeQueue<PacketQueue*>		_packetQueue;
	FuncProcess						_funcProcess[100];

	//note - connect 관련
public:
	bool				Initialize();
	bool				CreateAndConnect(const FString& ip, const int32 port);
	void				DoRecv();
	bool				CloseSocket();

	UFUNCTION(BlueprintCallable, Category = "A3_CLIENT")
		bool				StartServer(const FString& ip, const int32 port);

	UFUNCTION(BlueprintCallable, Category = "A3_CLIENT")
		FString				SendToServer(const int32 packetType, const FString &packetData);

	UFUNCTION(BlueprintCallable, Category = "A3_CLIENT")
		bool				DisConnect();

protected:
	FSocket*			_socket;
	bool				_connectState;

	//note - Thread 관련
public:
	//thread 관련 멤버변수 상속
	virtual	bool		Init() {
		return true;
	};
	virtual	uint32		Run() override;
	virtual	void		Stop()	override {};

protected:
	FRunnableThread*	_listenThread;
	bool				_isRun;

public:
	static	AA3_Connector*	_currentInstance;

	UFUNCTION(BlueprintCallable, Category = "A3_Connector")
		static	void			SetInstance(AActor* currentActor);
	static	AA3_Connector*	Getinstance();
};

#define A3_CONNECTOR	AA3_Connector::Getinstance()

enum PacketType
{
	CreateAccountPacketType = 10,
	LoginPacketType = 11,

	ChararterBroadCast = 22,
	CharacterBroadCastCreate = 23
};


// Fill out your copyright notice in the Description page of Project Settings.

#include "A3.h"
//
#include <String.h>
#include "A3_ProcessPacket.h"
//
#include "A3_Connector.h"

AA3_Connector* AA3_Connector::_currentInstance = NULL;

void AA3_Connector::SetInstance(AActor* currentActor)
{
	if (_currentInstance == NULL)
	{
		_currentInstance = Cast<AA3_Connector>(currentActor);
	}
}

AA3_Connector* AA3_Connector::Getinstance()
{
	if (_currentInstance != NULL)
	{
		return _currentInstance;
	}

	return NULL;
}

// Sets default values
AA3_Connector::AA3_Connector()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Initialize();
}

AA3_Connector::~AA3_Connector()
{
	_isRun = false;

// 	if (_listenThread)
// 	{
// 		_listenThread->WaitForCompletion();
// 		_listenThread->Kill();
// 
// 		delete _listenThread;
// 	}

	if (_currentInstance != NULL)
	{
		_currentInstance = NULL;
	}

// 	if (_socket->GetConnectionState() == SCS_Connected)
// 	{
// 		_socket->Close();
// 		ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->DestroySocket(_socket);
// 	}
}

void AA3_Connector::Destroy()
{
	_isRun = false;

	if (_listenThread)
	{
		_listenThread->WaitForCompletion();
		_listenThread->Kill();

		delete _listenThread;
	}

	if (_socket->GetConnectionState() == SCS_Connected)
	{
		_socket->Close();
		ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->DestroySocket(_socket);
	}
}

// Called when the game starts or when spawned
void AA3_Connector::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AA3_Connector::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

bool AA3_Connector::FuncPacketInitialize()
{
	_funcProcess[PacketType::CreateAccountPacketType].funcProcessPacket = AA3_ProcessPacket::CreateAccount;
	_funcProcess[PacketType::LoginPacketType].funcProcessPacket = AA3_ProcessPacket::Login;
	_funcProcess[PacketType::ChararterBroadCast].funcProcessPacket = AA3_ProcessPacket::UsersUpdate;

	return true;
}

bool AA3_Connector::RecvImmediately(int32 size, char* packetData)
{
	int	type = 0;
	memcpy(&type, packetData + 4, 2);

	if (type >= 0 && type < 100 && NULL != _funcProcess[type].funcProcessPacket)
	{
		//recv 받은 패킷의 데이터를 json형태로 형변화 시킴
		char buff[1024];
		memcpy(&buff, packetData + 6, size - 6);
		FString strBuf = buff;
		TSharedRef<TJsonReader<TCHAR>> reader = TJsonReaderFactory<>::Create(strBuf);
		TSharedPtr<FJsonObject> jsonData;

		FJsonSerializer::Deserialize(reader, jsonData);

		PacketQueue* packet = new PacketQueue();
		packet->_type = type;
		packet->_data = jsonData;
		
		_currentInstance->_packetQueue.Push(packet);

	//	PacketQueue* test = _packetQueue.Pop();
	
	//	_funcProcess[type].funcProcessPacket(jsonData);
		return true;
	}

	return false;
}

void AA3_Connector::ProcessPacketEvent()
{
	_currentInstance->_packetQueue.Lock();

	if (_currentInstance->_packetQueue.Empty() == 0)
	{
		_currentInstance->_packetQueue.UnLock();
		return;
	}

	PacketQueue* packet = _currentInstance->_packetQueue.Pop();
	if (packet->_type >= 100 || packet->_type <= 0 )
	{
		_currentInstance->_packetQueue.UnLock();
		return;
	}

	_funcProcess[packet->_type].funcProcessPacket(packet->_data);
	_currentInstance->_packetQueue.Erase(packet);

	_currentInstance->_packetQueue.UnLock();
}

bool AA3_Connector::Initialize()
{
	_connectState = false;
	_isRun = false;

	_socket = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateSocket(NAME_Stream, TEXT("default"), false);

	FuncPacketInitialize();

	return true;
}

bool AA3_Connector::CreateAndConnect(const FString& ip, const int32 port)
{
	if (!_socket)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Create Socket : Failed")));
		return false;
	}

	//address set
	FIPv4Address __ip;
	FIPv4Address::Parse(ip, __ip);

	TSharedRef<FInternetAddr> __address = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateInternetAddr();
	__address->SetIp(__ip.Value);
	__address->SetPort(port);

	//connection
	if (!_socket->Connect(*__address))
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Connect socket : Failed");
		return false;
	}

	_connectState = true;
	return true;
}

void AA3_Connector::DoRecv()
{
	while (_isRun)
	{
		char recvData[1024];
		FMemory::Memzero(recvData);

		int32	sent;
		bool	pending;

		if (_socket->HasPendingConnection(pending) & pending)
		{
			if (_socket->Recv((uint8*)recvData, 1024, sent))
			{
				if (sent <= 0)
				{
					GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString("Recv Error"));
					continue;
				}

				if (!RecvImmediately(sent, &recvData[0]))
				{
					FMemory::Memzero(recvData);
					sent = 0;
					continue;
				}

				//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, recvData);
				FMemory::Memzero(recvData);
				sent = 0;
			}
		}
	}
}

bool AA3_Connector::CloseSocket()
{
	if (_socket)
	{
		_socket->Close();
		ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->DestroySocket(_socket);
	}


	return true;
}

bool AA3_Connector::StartServer(const FString& ip, const int32 port)
{
	//Not Create!
	if (!CreateAndConnect(ip, port))
	{
		return false;
	}

	_isRun = true;
	_listenThread = FRunnableThread::Create(this, TEXT("Recv"), false, false, 0, TPri_BelowNormal);

	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, FString::Printf(TEXT("Connection : Success")));
	return true;
}

FString AA3_Connector::SendToServer(const int32 packetType, const FString &packetData)
{
	uint8	sendData[1024];
	//ZeroMemory(sendData, 1024);
	FMemory::MemZero(sendData);

	int32	sent;
	//	int		size = FString(packetData.GetCharArray().GetData()).Len() *sizeof(TCHAR);

	
	const char*	databuf = nullptr;
	auto convertor = StringCast<ANSICHAR>(packetData.GetCharArray().GetData());
	databuf = convertor.Get();

	//databuf = TCHAR_TO_ANSI(packetData.GetCharArray().GetData());

	int		datasize = strlen(databuf) * sizeof(char);

	int		packetSize = 4 + 2 + datasize;

	int		packetSizeBuf = sizeof(int32);
	int		packetTypeBuf = sizeof(short);

	memcpy(sendData, &packetSize, packetSizeBuf);
	memcpy(sendData + packetSizeBuf, &packetType, packetTypeBuf);
	memcpy(sendData + packetSizeBuf + packetTypeBuf, databuf, datasize);

	_socket->Send((uint8*)sendData, packetSize, sent);

	if (sent <= 0)
	{
		return FString("FAIL");
	}
	sent = 0;
	databuf = nullptr;
	//DoRecv();
	return FString("Success");
}

bool AA3_Connector::DisConnect()
{
	if (_socket->Close())
	{
		Initialize();
		return true;
	}
	
	return false;
}

uint32 AA3_Connector::Run()
{
	DoRecv();
	return 0;
}


// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameModeBase.h"
#include "A3_GameMode.generated.h"

/**
*
*/

//note - packet delegate
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FPacketEventToBind, AActor*, pActor, int32, pType, FString, pData);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FCharEventDelegate, AActor*, pActor, int32, pType, bool, existence, FString, pData);

UCLASS()
class A3_API AA3_GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AA3_GameMode();
	~AA3_GameMode();

public:
	void			OnDelegate(FString hashTag, const int32 packetType, const FString packetData);
	void			SetDelegate();

	void			OnCharacterDelegate(FString hashTag, const int32 packetType, const FString packetData);

public:
	UPROPERTY(BlueprintAssignable, Category = "A3_GameMode")
	FPacketEventToBind			_PEventToBind;

	UPROPERTY(BlueprintAssignable, Category = "A3_GameMode")
	FCharEventDelegate			_charactorDelegate;

public:
	FCriticalSection			_cs;

	// note - HashTable 
	// key - FString
	// value - Actor
protected:
	TMap<FString, AActor*>		_hashTable;
	TMap<FString, AActor*>		_backupHashTable;

public:
	UFUNCTION(BlueprintCallable, Category = "A3_HashTable")
	void	SetHashTable(const FString hashTag, AActor* hashData);
	AActor*	GetHashTable(const FString hashTag);

	UFUNCTION(BlueprintCallable, Category = "A3_HashTable")
	void	SetBackupHashTable(const FString hashTag, AActor* hashData);
	AActor*	GetBackupHashTable(const FString hashTag);

	//note - InGame PlayerControllerClass Select
public:
	UFUNCTION(BlueprintCallable, Category = "A3_GameMode")
	void	SetPlayerController(ACharacter* characterActor);

	//My Character Data Update To Server
public:
	void	UpdateToServer();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "A3_Timer")
		void	SetCharacterInfo(AActor* actor);
	virtual void SetCharacterInfo_Implementation(AActor* actor);

	UFUNCTION(BlueprintCallable, Category = "A3_Timer")
	void	TimerOn();

};

// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "A3.h"
#include "A3GameMode.h"
#include "A3Character.h"

AA3GameMode::AA3GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

// Fill out your copyright notice in the Description page of Project Settings.

#include "A3.h"
#include "Shard.h"


// Sets default values
AShard::AShard()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AShard::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AShard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector NewLocation = GetActorLocation();
	float DeltaHeight = (FMath::Sin(RunningTime + DeltaTime) - FMath::Sin(RunningTime)) * 5;
	NewLocation.Z += DeltaHeight * 5.0f;
	RunningTime += DeltaTime;
	SetActorLocation(NewLocation);
}


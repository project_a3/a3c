// Fill out your copyright notice in the Description page of Project Settings.

#include "A3.h"

#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "A3_Helper.h"
#include "A3_Connector.h"

#include "BaseCharacter.h"


void ABaseCharacter::SetIndex(const int32 index)
{
	_index = index;
}

void ABaseCharacter::SetCharacterType(const int32 characterType)
{
	_characterType = characterType;
}

void ABaseCharacter::SetCharacterState(const int32 characterState)
{
	_state = characterState;
}

void ABaseCharacter::SetCharacterInfo(const FString& data)
{
	TSharedRef<TJsonReader<TCHAR>> reader = TJsonReaderFactory<>::Create(data);
	TSharedPtr<FJsonObject> jsonData;
	FJsonSerializer::Deserialize(reader, jsonData);

	_index = jsonData->GetIntegerField("Index");
	_characterType = jsonData->GetIntegerField("Type");
	_state = jsonData->GetIntegerField("State");
	
	FVector pos;
	pos.X = jsonData->GetNumberField("X");
	pos.Y = jsonData->GetNumberField("Y");
	pos.Z = jsonData->GetNumberField("Z");
	this->SetActorLocation(pos);

	FRotator rotate;
	rotate.Roll = jsonData->GetNumberField("Roll");
	rotate.Pitch = jsonData->GetNumberField("Pitch");
	rotate.Yaw = jsonData->GetNumberField("Yaw");
	this->SetActorRotation(rotate);
}

// Sets default values
ABaseCharacter::ABaseCharacter()
{
	// CameraZoom
	CameraZoom_v = 300.0;

	// 캡슐 크기 지정
	GetCapsuleComponent()->InitCapsuleSize(20.f, 28.0f);

	// Turn 각 설정
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// 컨트롤러 돌아갈때 돌리면 오류 발생, 필수 고정
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// character movement
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f);
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 100.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//index, type;
	_index = 0;
	_state = 0;
}

ABaseCharacter::~ABaseCharacter()
{
	_index = 0;
	_state = 0;
}

// Called to bind functionality to input
void ABaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("Run", IE_Pressed, this, &ABaseCharacter::StartRunning);
	PlayerInputComponent->BindAction("Run", IE_Released, this, &ABaseCharacter::StopRunning);

	PlayerInputComponent->BindAction("ZoomIn", IE_Pressed, this, &ABaseCharacter::CameraZoomIn);
	PlayerInputComponent->BindAction("ZoomOut", IE_Pressed, this, &ABaseCharacter::CameraZoomOut);

	PlayerInputComponent->BindAxis("MoveForward", this, &ABaseCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ABaseCharacter::MoveRight);

	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
}

// Implement the CalculateHealth function
void ABaseCharacter::CalculateHealth(float Delta)
{
	Health += Delta;
	CalculateDead();
}

// Implement the CalculateDead function
void ABaseCharacter::CalculateDead()
{
	if (Health <= 0)
		isDead = true;
	else
		isDead = false;

}
#if WITH_EDITOR
// Implement the remainder of our helper code, used by the editor when we change values.
void ABaseCharacter::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	isDead = false;
	Health = 100;

	Super::PostEditChangeProperty(PropertyChangedEvent);

	CalculateDead();
}
#endif

//// JoyStick은 시점이 반대
//void ABaseCharacter::TurnAtRate(float Rate)
//{
//	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
//}
//
//void ABaseCharacter::LookUpAtRate(float Rate)
//{
//	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
//}

void ABaseCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// 앞 찾기
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// 전방 벡터
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ABaseCharacter::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		AddMovementInput(Direction, Value);
	}
}

void ABaseCharacter::CameraZoomIn()
{
	float a = 25.0;
	CameraZoom_v = CameraZoom_v - 25.0;

	if (CameraZoom_v <= 75.0)
	{
		CameraBoom->TargetArmLength = 75.0;
		CameraZoom_v = 75.0;
	}
	else
	{
		CameraBoom->TargetArmLength = CameraZoom_v;
	}
}

void ABaseCharacter::CameraZoomOut()
{
	float a = 25.0;
	CameraZoom_v = CameraZoom_v + 25.0;

	if (CameraZoom_v >= 300.0)
	{
		CameraBoom->TargetArmLength = 300.0;
		CameraZoom_v = 300.0;
	}
	else
	{
		CameraBoom->TargetArmLength = CameraZoom_v;
	}
}

void ABaseCharacter::StartRunning()
{
	bPressedRun = true;
	RunKeyHoldTime = 0.0f;
	GetCharacterMovement()->MaxWalkSpeed = RunSpeed;
}

void ABaseCharacter::StopRunning()
{
	bPressedRun = false;
	GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;
}
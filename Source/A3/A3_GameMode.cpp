// Fill out your copyright notice in the Description page of Project Settings.

#include "A3.h"

#include "A3_Helper.h"
#include "A3_Connector.h"
#include "BaseCharacter.h"

#include "A3_GameMode.h"

AA3_GameMode::AA3_GameMode()
{
	_PEventToBind.Clear();
	_charactorDelegate.Clear();
	//	SetDelegate();


}

AA3_GameMode::~AA3_GameMode()
{
	_PEventToBind.RemoveAll(this);
	_charactorDelegate.RemoveAll(this);
}

void AA3_GameMode::OnDelegate(FString hashTag, const int32 packetType, const FString packetData)
{
	AActor* actorInstance = GetHashTable(hashTag);
	int32 pType = packetType;
	if (packetType == ChararterBroadCast)
	{		
		if (!actorInstance)
		{
			actorInstance = GetHashTable("BaseCharacter");
			pType = CharacterBroadCastCreate;
		}
	}
	if (_PEventToBind.IsBound())
	{
		_PEventToBind.Broadcast(actorInstance, pType, packetData);
	}
	
}

void AA3_GameMode::SetDelegate()
{
	// 	TScriptDelegate<FWeakObjectPtr> delegateFunc;7
	// 	delegateFunc.BindUFunction(this, "PacketEventCall");
	// 	_PEventToBind.Add(delegateFunc);	
}

void AA3_GameMode::OnCharacterDelegate(FString hashTag, const int32 packetType, const FString packetData)
{
	AActor* actorInstance = GetHashTable(hashTag);
	bool existence = true;
	if (!actorInstance)
	{
		actorInstance = GetBackupHashTable(hashTag);
		existence = false;
	}

	if (_charactorDelegate.IsBound())
	{
		_charactorDelegate.Broadcast(actorInstance, packetType, existence, packetData);
	}
}

void AA3_GameMode::SetHashTable(const FString hashTag, AActor* hashData)
{
	if (hashData != NULL)
	{
		_hashTable.Add(hashTag, hashData);
	}
}

AActor* AA3_GameMode::GetHashTable(const FString hashTag)
{
	AActor* instance = NULL;

	instance = _hashTable.FindRef(hashTag);
	if (instance != nullptr)
	{
		return instance;
	}

	return NULL;
}

void AA3_GameMode::SetBackupHashTable(const FString hashTag, AActor* hashData)
{
	if (hashData != NULL)
	{
		_backupHashTable.Add(hashTag, hashData);
	}
}

AActor* AA3_GameMode::GetBackupHashTable(const FString hashTag)
{
	AActor* instance = NULL;

	instance = _backupHashTable.FindRef(hashTag);
	if (instance != nullptr)
	{
		return instance;
	}

	return NULL;
}

void AA3_GameMode::SetPlayerController(ACharacter* characterActor)
{
	DefaultPawnClass = characterActor->GetActorClass();
}

void AA3_GameMode::UpdateToServer()
{
	//character info
	ABaseCharacter* characterInstance = Cast<ABaseCharacter>(_hashTable.FindRef("MyCharacter"));
	if (!characterInstance)
	{
		return;
	}

//	SetCharacterInfo(characterInstance);

	FVector locate = characterInstance->GetActorLocation();
	FRotator rotate = characterInstance->GetActorRotation();

	//helper
	AA3_Helper* helper = Cast<AA3_Helper>(_hashTable.FindRef("A3_Helper"));
	if (!helper)
	{
		return;
	}

	helper->SetInt("Type", characterInstance->GetCharacterType());
	helper->SetInt("Index", characterInstance->GetIndex());
	helper->SetInt("State", characterInstance->GetCharacterState());
	
	helper->SetFloat("X", locate.X);
	helper->SetFloat("Y", locate.Y);
	helper->SetFloat("Z", locate.Z);

 	helper->SetFloat("Roll", rotate.Roll);
 	helper->SetFloat("Pitch", rotate.Pitch);
	helper->SetFloat("Yaw", rotate.Yaw);

	//const FString packetData = helper->ParseToJsonDate();

	AA3_Connector* connector = Cast<AA3_Connector>(_hashTable.FindRef("A3_Connector"));
	if (connector)
	{
		connector->SendToServer(CHARACTERUPDATE, helper->ParseToJsonDate());
	}
}

void AA3_GameMode::SetCharacterInfo_Implementation(AActor* actor)
{
}

void AA3_GameMode::TimerOn()
{
	FTimerHandle characterTimerHandle;
	GetWorldTimerManager().SetTimer(characterTimerHandle, this, &AA3_GameMode::UpdateToServer, 0.04f, true);
}

// void AA3_GameMode::PacketEventCall(const int32 packetType, const FString packetData)
// {
// 	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Create"));
// }



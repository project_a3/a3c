// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"

#include "Json.h"
#include "A3_GameMode.h"

#include "A3_ProcessPacket.generated.h"

UCLASS()
class A3_API AA3_ProcessPacket : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AA3_ProcessPacket();
	~AA3_ProcessPacket();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	//note - packet 처리
public:
	static void						CreateAccount(TSharedPtr<FJsonObject> packetData);
	static void						Login(TSharedPtr<FJsonObject> packetData);
	static void						UsersUpdate(TSharedPtr<FJsonObject> packetData);

	//note - actor를 통해 world를 가져오기 위함
public:
	static UWorld*					GetWorldPoint();
	static AA3_GameMode*			GetGameMode();

	UFUNCTION(BlueprintCallable, Category = "A3ProcessPacket")
		void						SetCurrentGameMode(AActor* currentActor);

protected:
	static AA3_ProcessPacket*		_worldInstance;
};
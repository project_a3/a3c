// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "BaseCharacter.generated.h"

UCLASS(Blueprintable)
class A3_API ABaseCharacter : public ACharacter/*, public FRunnable*/
{
	GENERATED_BODY()

	// Camera Boom 위치 지정
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

	// 카메라 따라오기
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FollowCamera;

	//note - character index 관리 
public:
	UFUNCTION(BlueprintCallable, Category = "A3_Character")
	void	SetIndex(const int32 index);
	UFUNCTION(BlueprintCallable, Category = "A3_Character")
	int32	GetIndex() { return _index; }

	UFUNCTION(BlueprintCallable, Category = "A3_Character")
	void	SetCharacterType(const int32 characterType);
	UFUNCTION(BlueprintCallable, Category = "A3_Character")
	int32	GetCharacterType() { return _characterType; }

	UFUNCTION(BlueprintCallable, Category = "A3_Character")
	void	SetCharacterState(const int32 characterState);
	UFUNCTION(BlueprintCallable, Category = "A3_Character")
	int32	GetCharacterState() { return _state; }

public:
	UFUNCTION(BlueprintCallable, Category = "A3_Character")
	void	SetCharacterInfo(const FString& data);

protected:
	int32				_index;
	int32				_characterType;
	int32				_state;

public:
	// Sets default values for this character's properties
	ABaseCharacter();
	~ABaseCharacter();

	// 시점 좌우
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	// 시점 상하
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;

	// 달리기
	UPROPERTY(BlueprintReadOnly, Category = "Pawn|Character")
		uint32 bPressedRun : 1;

	UPROPERTY(Transient, BlueprintReadOnly, VisibleInstanceOnly, Category = Character)
		float RunKeyHoldTime;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Base Character")
		float RunSpeed = 100;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Base Character")
		float WalkSpeed = 25;


public:
	// Health property
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Base Character")
		float Health = 100;

	// isDead property
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Base Character")
		bool isDead = false;

	// Calculate death function (helper)
	virtual void CalculateDead();

	// Health Calculate Function
	UFUNCTION(BlueprintCallable, Category = "Base Character")
		virtual void CalculateHealth(float delta);

#if WITH_EDITOR
	// Editor-centric code for chaging properties
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

protected:
	void MoveForward(float Value);

	void MoveRight(float Value);

protected:
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

protected:
	void CameraZoomIn();
	void CameraZoomOut();
	float CameraZoom_v;
	void StartRunning();
	void StopRunning();

public:
	FORCEINLINE class USpringArmComponent* GetCmeraBoom() const { return CameraBoom; }

	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
};

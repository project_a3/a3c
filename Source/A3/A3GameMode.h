// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "A3GameMode.generated.h"

UCLASS(minimalapi)
class AA3GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AA3GameMode();
};




// Fill out your copyright notice in the Description page of Project Settings.

#include "A3.h"

//
#include "A3_GameMode.h"
#include "A3_Connector.h"
//

#include "A3_ProcessPacket.h"


AA3_ProcessPacket* AA3_ProcessPacket::_worldInstance = NULL;
// Sets default values
AA3_ProcessPacket::AA3_ProcessPacket()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// 	if (_worldInstance == NULL)
	// 	{
	// 		_worldInstance = this;
	// 	}
}

AA3_ProcessPacket::~AA3_ProcessPacket()
{
	_worldInstance = NULL;
}

// Called when the game starts or when spawned
void AA3_ProcessPacket::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AA3_ProcessPacket::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AA3_ProcessPacket::CreateAccount(TSharedPtr<FJsonObject> _packetData)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, _packetData->GetStringField("errorCode"));
}

void AA3_ProcessPacket::Login(TSharedPtr<FJsonObject> _packetData)
{
	TSharedPtr<FJsonObject> resultData = _packetData->GetObjectField("result");
	FString accountIndex = resultData->GetStringField("accountIndex");

	GetGameMode()->OnDelegate("A3Connector", LoginPacketType, accountIndex);
// 	UWorld* world = GetWorldPoint();
// 	if (!world)
// 	{
// 		return;
// 	}
// 
// 	AA3_GameMode* gameMode = world->GetAuthGameMode<AA3_GameMode>();
// 	if (!gameMode)
// 	{
// 		return;
// 	}
// 	gameMode->OnDelegate("A3Connector", 11, accountIndex); 
}

void AA3_ProcessPacket::UsersUpdate(TSharedPtr<FJsonObject> packetData)
{
	TSharedPtr<FJsonObject> resultData = packetData->GetObjectField("result");
	TArray<TSharedPtr<FJsonValue>> characters = resultData->GetArrayField("UserInfo");
	
	FString str;

	FString charName[4];
	charName[0] = "GunMan";
	charName[1] = "SwordMan";
	charName[2] = "later";
	charName[3] = "later";

	for (int i = 0; i < characters.Max(); i++)
	{
		TSharedPtr<FJsonValue> value = characters[i];
		TSharedPtr<FJsonObject> json = value->AsObject();
	
		FString buff;
		TSharedRef<TJsonWriter<TCHAR>> writer = TJsonWriterFactory<TCHAR>::Create(&buff);
		FJsonSerializer::Serialize(json.ToSharedRef(), writer);

 		int32 charIndex = json->GetIntegerField("Index");
 		int32 charType = json->GetIntegerField("Type");
// 		int32 charState = json->GetIntegerField("State");
// 
// 		float x = json->GetNumberField("X");
// 		float y = json->GetNumberField("Y");
// 		float z = json->GetNumberField("Z");
// 
// 		float roll = json->GetNumberField("Roll");
// 		float pitch = json->GetNumberField("Pitch");
// 		float yaw = json->GetNumberField("Yaw");
		
		GetGameMode()->OnDelegate(FString::FromInt(charIndex), ChararterBroadCast, buff);
		//GetGameMode()->OnCharacterDelegate(charName[charType], charType, buff);
	}

	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, str);
	FMemory::MemZero(str);
}

AA3_GameMode* AA3_ProcessPacket::GetGameMode()
{
	UWorld* world = GetWorldPoint();
	if (!world)
	{
		return NULL;
	}

	AA3_GameMode* gameMode = world->GetAuthGameMode<AA3_GameMode>();
	if (!gameMode)
	{
		return NULL;
	}
	return gameMode;
}

// void AA3_ProcessPacket::DelegateCall(FString actorName, int32 packetType, FString packetData)
// {
// 	UWorld* world = GetWorldPoint();
// 	if (!world)
// 	{
// 		return;
// 	}
// 
// 	AA3_GameMode* gameMode = world->GetAuthGameMode<AA3_GameMode>();
// 	if (!gameMode)
// 	{
// 		return;
// 	}
// 	gameMode->OnDelegate(actorName, packetType, packetData);
// }

UWorld* AA3_ProcessPacket::GetWorldPoint()
{
	if (!_worldInstance)
	{
		return NULL;
	}

	return _worldInstance->GetWorld();
}

void AA3_ProcessPacket::SetCurrentGameMode(AActor* currentActor)
{
	if (_worldInstance == NULL)
	{
		_worldInstance = (AA3_ProcessPacket*)currentActor;
	}
}

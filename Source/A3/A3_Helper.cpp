// Fill out your copyright notice in the Description page of Project Settings.

#include "A3.h"
#include "A3_Helper.h"


// Sets default values
AA3_Helper::AA3_Helper()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	if (_jsonData.IsValid())
	{
		_jsonData.Reset();
	}

	_jsonData = MakeShareable(new FJsonObject());
}

// Called when the game starts or when spawned
void AA3_Helper::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AA3_Helper::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

FString AA3_Helper::ParseToJsonDate()
{
	FString __str;

	TSharedRef<TJsonWriter<TCHAR>> writer = TJsonWriterFactory<TCHAR>::Create(&__str);
	
	FJsonSerializer::Serialize(_jsonData.ToSharedRef(), writer);

	_jsonData->Values.Empty();

	return __str;
}

AA3_Helper* AA3_Helper::SetString(const FString& key, const FString& value)
{
	_jsonData->SetStringField(*key, *value);
	return this;
}

AA3_Helper* AA3_Helper::SetInt(const FString& key, const int32 value)
{
	_jsonData->SetNumberField(*key, value);
	return this;
}

AA3_Helper* AA3_Helper::SetFloat(const FString& key, const float value)
{
	_jsonData->SetNumberField(*key, value);
	return this;
}

AA3_Helper* AA3_Helper::SetBool(const FString& key, const bool value)
{
	_jsonData->SetBoolField(*key, value);
	return this;
}

AA3_Helper* AA3_Helper::JsonConvertor(const FString& data)
{
	FString strBuf = data;
	TSharedRef<TJsonReader<TCHAR>> reader = TJsonReaderFactory<>::Create(strBuf);

	FJsonSerializer::Deserialize(reader, _inputJsonData);

	return this;
}

int AA3_Helper::GetInt(const FString& key)
{
	return _inputJsonData->GetIntegerField(key);
}

float AA3_Helper::GetFloat(const FString& key)
{
	return _inputJsonData->GetNumberField(key);
}

FString AA3_Helper::GetString(const FString& key)
{
	return _inputJsonData->GetStringField(key);
}

bool AA3_Helper::GetBool(const FString& key)
{
	return _inputJsonData->GetBoolField(key);
}

void AA3_Helper::ReleaseJson()
{
	_jsonData->Values.Empty();
}

